# Detect Handwritten numbers

This is a sample Android application to detect handwritten numbers powered by Tensorflow.

A sample result as below - 

![Sample Result](https://i.ibb.co/K7YdD0R/Result.png)
